// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <iostream>

#include <getopt.h>
#include <syslog.h>

#include <sodium.h>

#include "hellmouth/log/ostream.h"
#include "hellmouth/db/types.h"
#include "hellmouth/db/node.h"

using namespace hellmouth;

/** Print help text.
 * @param out the ostream to which the help text should be written
 */
void write_help(std::ostream& out) {
	out << "Usage: hellmouth db daemon [<options>]"
		<< std::endl;
	out << std::endl;
	out << "Options:" << std::endl;
	out << std::endl;
	out << "  -h  display this help text then exit" << std::endl;
	out << "  -v  increase the verbosity level (repeatable)" << std::endl;
	out << "  -D  the directory for storing the database" << std::endl;
	out << "  -N  the node ID" << std::endl;
	out << "  -U  the database UNIX domain socket" << std::endl;
}

int main(int argc, char* argv[]) {
	// [TODO]: initialisation should be handled implicitly
	// within hellmouth.so.
	sodium_init();

	db::config cfg;
	int verbosity = LOG_NOTICE;

	int opt;
	while ((opt = getopt(argc, argv, "hvD:N:U:")) != -1) {
		switch (opt) {
		case 'h':
			write_help(std::cout);
			return 0;
		case 'v':
			verbosity += 1;
			break;
		case 'D':
			cfg.dirname = optarg;
			break;
		case 'N':
			cfg.node_id = atol(optarg);
			break;
		case 'U':
			cfg.sockpath = optarg;
			break;
		}
	}

	if (verbosity > LOG_DEBUG) {
		verbosity = LOG_DEBUG;
	}
	log::ostream logger(verbosity, std::cerr);
	cfg.logger = &logger;

	db::node _node(cfg);
	_node.run();
}
