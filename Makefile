# This file is part of the HELLMOUTH cluster system.
# Copyright 2024 Graham Shaw.
# Distribution and modification are permitted within the terms of the
# GNU General Public License (version 3 or any later version).

prefix = /usr/local
exec_prefix = $(prefix)
libdir = $(exec_prefix)/lib
libexecdir = $(libdir)
includedir = $(exec_prefix)/include

pkgname = hellmouth

CPPFLAGS = -MD -MP -I.
CXXFLAGS = -fPIC -O2 --std=c++2a -Wall -Wpedantic
LDLIBS = -latomic -lsodium $(libdir)/hellmouth.so
TESTLIBS = -lCatch2Main -lCatch2

SRC = $(wildcard src/*.cc)
AUXBIN = $(SRC:src/%.cc=bin/%)

HELLMOUTH = $(wildcard hellmouth/*.cc) $(wildcard hellmouth/*/*.cc) $(wildcard hellmouth/*/*/*.cc)
TEST = $(wildcard test/*.cc) $(wildcard test/*/*.cc) $(wildcard test/*/*/*.cc)

INCLUDE = $(wildcard hellmouth/*.h) $(wildcard hellmouth/*/*.h) $(wildcard hellmouth/*/*/*.h)

.PHONY: all
all: hellmouth-db.so $(AUXBIN) bin/test

hellmouth-db.so: $(HELLMOUTH:%.cc=%.o)
	gcc -shared -o $@ $^ $(LDLIBS)

$(AUXBIN): bin/%: src/%.o hellmouth-db.so
	@mkdir -p bin
	g++ -rdynamic -Wl,-rpath $(libdir) -o $@ $^ $(LDLIBS)

bin/test: $(TEST:%.cc=%.o) $(HELLMOUTH:%.cc=%.o)
	@mkdir -p bin
	g++ -rdynamic -Wl,-rpath $(libdir) -o $@ $^ $(LDLIBS) $(TESTLIBS)

.PHONY: clean
clean:
	rm -f hellmouth/*.[do]
	rm -f hellmouth/*/*.[do]
	rm -f hellmouth/*/*/*.[do]
	rm -f src/*.[do]
	rm -f *.so
	rm -rf bin

$(includedir)/%.h: %.h
	@mkdir -p $(dir $@)
	cp $< $(dir $@)

.PHONY: install
install: $(INCLUDE:%.h=$(includedir)/%.h)
	@mkdir -p $(libexecdir)/$(pkgname)/bin
	@mkdir -p $(libdir)
	cp $(AUXBIN) $(libexecdir)/$(pkgname)/bin/
	cp hellmouth-db.so $(libdir)/

.PHONY: uninstall
uninstall:
	rm -f $(INCLUDE:%.h=$(includedir)/%.h)
	find $(includedir)/hellmouth -type d -empty -delete
	rm $(libdir)/hellmouth-db.so
	rm -rf $(libexecdir)/$(pkgname)

.PHONY: always
always:

-include $(HELLMOUTH:%.cc=%.d)
-include $(SRC:%.cc=%.d)
