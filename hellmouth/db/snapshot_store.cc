// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <string>
#include <format>
#include <regex>
#include <fstream>

#include <fcntl.h>

#include "hellmouth/os/file_descriptor.h"
#include "hellmouth/cbor/encoder.h"
#include "hellmouth/cbor/decoder.h"
#include "hellmouth/db/snapshot_store.h"

namespace hellmouth::db {

std::filesystem::path snapshot_store::make_pathname(
	index_type index, bool temporary) const {

	const char* prefix = (temporary) ? "." : "";
	return _dirname / std::format("{}{:016x}.db", prefix, index);
}

snapshot_store::snapshot_store(const std::filesystem::path& dirname):
	_dirname(dirname) {

	static const std::regex filename_pattern(R"([0-9a-f]{16}[.]db)");

	std::filesystem::create_directory(_dirname);
	for (const auto& entry: std::filesystem::directory_iterator(_dirname)) {
		std::string filename = entry.path().filename().string();
		if (regex_match(filename, filename_pattern)) {
			std::string hexindex(filename, 0, 16);
			index_type index = strtoull(hexindex.c_str(), 0, 16);
			if (index > _last_snapshot_index) {
				_last_snapshot_index = index;
			}
		}
	}
}

data::any snapshot_store::read(index_type index) const {
	std::ifstream in(make_pathname(index));
	cbor::decoder dec(in);
	return dec();
}

void snapshot_store::write(index_type index, const data::any& content) {
	std::lock_guard lock(_mutex);

	std::filesystem::path temp_pathname = make_pathname(index, true);
	os::file_descriptor fd(temp_pathname,
		O_WRONLY | O_CREAT | O_TRUNC, 0600);
	{
		std::ofstream out(temp_pathname);
		cbor::encoder enc(out);
		enc(content);
	}
	fd.fdatasync();

	std::filesystem::path pathname = make_pathname(index, false);
	std::filesystem::rename(temp_pathname, pathname);
	os::file_descriptor dirfd(_dirname, O_RDONLY);
	dirfd.fsync();

	if (index > _last_snapshot_index) {
		_last_snapshot_index = index;
	}
}

}
