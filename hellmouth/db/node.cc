// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <algorithm>
#include <sstream>
#include <thread>

#include "hellmouth/data/pointer.h"
#include "hellmouth/cbor/encoder.h"
#include "hellmouth/db/error.h"
#include "hellmouth/db/encoder.h"
#include "hellmouth/db/decoder.h"
#include "hellmouth/db/changeset.h"
#include "hellmouth/db/message/nomination.h"
#include "hellmouth/db/message/vote.h"
#include "hellmouth/db/message/replication.h"
#include "hellmouth/db/message/anchor.h"
#include "hellmouth/db/message/proposal.h"
#include "hellmouth/db/node.h"

namespace hellmouth::db {

static const size_t max_fragment_size = 1024;

data::any node::get(std::string_view path) {
	try {
		data::pointer ptr(path);
		data::any root = _state.content();
		const data::any& croot = root;
		return ptr(croot);
	} catch (std::out_of_range&) {
		throw db::not_found_error("path not found");
	} catch (std::invalid_argument&) {
		throw db::not_found_error("path not found");
	}
}

void node::patch(const data::any& spec, bool nowait) {
	changeset cset(spec, _keypair);
	auto sig = cset.signature();
	auto& notification = _state.track(sig);

	for (auto& fragment : cset.fragments(max_fragment_size)) {
		message::proposal prop(_current_term, _cfg.node_id, std::move(fragment));
		if (_mode == operating_mode::leader) {
			handle(prop);
		} else {
			db::encoder enc;
			enc << prop;
			_bus.send(enc.data());
		}
	}

	changeset_notification::outcome_type outcome = nowait ?
		changeset_notification::success :
		notification.await();
	_state.untrack(sig);

	if (outcome != changeset_notification::success) {
		throw patch_failed_error(
			"patch could not be applied to database");
	}
}

void node::patch(std::string_view path, const data::any& spec) {
	data::any cset;
	data::any* node = &cset;
	for (const auto& segment : data::pointer(path)) {
		const std::string& key = segment;
		node = &(*node)[key];
	}
	*node = spec;
	patch(cset);
}

data::any node::status() {
	data::object last_status;
	last_status["index"] = _log.last_log_index();
	last_status["term"] = _log.last_log_term();

	data::object commit_status;
	commit_status["index"] = _commit_index;
	commit_status["term"] = _log.at(_commit_index).term();

	data::object status;
	status["log"] = last_status;
	status["commit"] = commit_status;
	status["mode"] = to_string(_mode);
	return status;
}

void node::accept_client_connection() {
	auto accept = [this](){
		try {
			_server.accept();
		} catch (std::ios_base::failure&) {}
	};

	// [TODO]: Fire-and-forget should work tolerably well under
	// typical conditions, but a more robust solution will likely
	// be wanted for production use.
	std::thread th(accept);
	th.detach();
}

void node::become_follower() {
	if (_mode == operating_mode::leader) {
		_cfg.logger->format(LOG_NOTICE,
			"Reverting fom leader to follower");
	}
	if (_mode != operating_mode::follower) {
		_mode = operating_mode::follower;
		_election_timeout.reset(_cfg.election_timeout_period);
	}
}

void node::become_leader() {
	if (_mode != operating_mode::leader) {
		_cfg.logger->format(LOG_NOTICE,
			"Elected as leader");
		_mode = operating_mode::leader;
		patch(data::any(data::object()), true);
		_heartbeat_timeout.reset(_cfg.heartbeat_timeout_period);
	}
}

void node::begin_new_term(term_type term) {
	_current_term = term;
	become_follower();
	_voted_for = 0;
	_votes.clear();
	_log.begin_new_term(term);
}

void node::handle(message::nomination& req) {
	if (req.term() < _current_term) {
		_cfg.logger->format(LOG_INFO,
			"Not voting for node {} in term {} (expired)",
			req.sender_id(), req.term());
		return;
	}

	if (_voted_for) {
		_cfg.logger->format(LOG_INFO,
			"Not voting for node {} in term {} (already voted)",
			req.sender_id(), req.term());
		return;
	}

	if (req.last_log_index() < _log.last_log_index() ||
		req.last_log_term() < _log.last_log_term()) {

		_cfg.logger->format(LOG_INFO,
			"Not voting for node {} in term {} (log not current)",
			req.sender_id(), req.term());
		return;
	}

	_voted_for = req.candidate_id();
	message::vote resp(_current_term, _cfg.node_id, _voted_for);
	db::encoder enc;
	enc << resp;
	_bus.send(enc.data());

	_cfg.logger->format(LOG_INFO,
		"Voted for node {} in term {}",
		req.sender_id(), req.term());
}

void node::handle(message::vote& req) {
        if (req.candidate_id() != _cfg.node_id) {
		_cfg.logger->format(LOG_DEBUG,
			"Vote cast by node {} for node {} in term {}",
			req.voter_id(), req.candidate_id(), req.term());
                return;
        }

        if (req.term() != _current_term) {
		_cfg.logger->format(LOG_INFO,
			"Rejected vote from node {} in term {} (expired)",
			req.voter_id(), req.term());
        }

        if (_mode != operating_mode::candidate) {
		_cfg.logger->format(LOG_WARNING,
			"Rejected vote from node {} in term {} "
			"(not a candidate)",
			req.voter_id(), req.term());
        }

	_votes.insert(req.voter_id());
	_cfg.logger->format(LOG_INFO,
		"Accepted vote from node {} in term {} ({}/{})",
		req.voter_id(), req.term(), _votes.size(), _cfg.quorum);

	if (_votes.size() >= _cfg.quorum) {
		become_leader();
	}
}

void node::handle(message::replication& req) {
	if (req.term() < _current_term) {
		_cfg.logger->format(LOG_INFO,
			"Disregarding replication message from expired term {}",
			req.term());
                return;
	}

	_election_timeout.reset(_cfg.election_timeout_period);
	_cfg.logger->format(LOG_DEBUG,
		"Reset election timeout to {}ms",
		_cfg.election_timeout_period);

	bool match = _log.append(
		req.anchor_index(), req.anchor_term(), req.entries());
	if (match) {
		index_type last_matching_index =
			req.anchor_index() + req.entries().size();
		index_type new_commit_index = std::min(
			req.leader_commit(), last_matching_index);
		if (new_commit_index > _commit_index) {
			_commit_index = new_commit_index;
			_cfg.logger->format(LOG_DEBUG,
				"Advanced commit index to {}",
				_commit_index);
			apply_committed_log_entries();
		}
		_log_cache.replay(_log);
	} else {
		_log_cache.insert(message::replication(req));
	}

	db::message::anchor resp(_current_term, _cfg.node_id,
		_log.anchor_index(), _log.anchor_term());
	db::encoder enc;
	enc << resp;
	_bus.send(enc.data());
}

void node::handle(message::anchor& req) {
	// If we do not possess the log entry to which the anchor message
	// refers then we cannot deduce anything from the supplied term number.
	if (req.anchor_index() > _log.last_log_index()) {
		return;
	}

	// Determine whether the message refers to an instance of successful
	// or unsuccessful replication.
	bool success = _log.at(req.anchor_index()).term() == req.anchor_term();

	node_id_type node_id = req.sender_id();
	if (success) {
		follower& fwr = _followers.at(node_id);
		fwr.handle_match(req.anchor_index());
	} else {
		if (req.anchor_index() < _log.last_log_index()) {
			replicate(req.anchor_index() + 1);
		}
	}

	update_commit_index();
}

void node::handle(message::proposal& prop) {
	if (_mode == operating_mode::leader) {
		log_entry entry(_current_term,
			changeset_fragment(prop.fragment()));

		index_type index = _log.append(std::move(entry));
		_cfg.logger->format(LOG_INFO,
			"New log entry at index {}", index);
		replicate(index);
	}
}

void node::handle(message::basic_message& msg) {
	if (_cfg.logger->severity() >= LOG_DEBUG) {
		const char* dirstr =
			(msg.sender_id() == _cfg.node_id) ?
			">> " : "<< ";
		_cfg.logger->format(LOG_DEBUG, "{} {}",
			dirstr, std::string(msg));
	}

	if (msg.sender_id() == _cfg.node_id) {
		return;
	}

	if (msg.term() > _current_term) {
		begin_new_term(msg.term());
	}

	node_id_type node_id = msg.sender_id();
	if (!_followers.contains(node_id)) {
		_followers.emplace(node_id, follower());
	}

	switch (msg.type()) {
	case message::message_type::nomination:
		handle(dynamic_cast<message::nomination&>(msg));
		break;
	case message::message_type::vote:
		handle(dynamic_cast<message::vote&>(msg));
		break;
	case message::message_type::replication:
		handle(dynamic_cast<message::replication&>(msg));
		break;
	case message::message_type::anchor:
		handle(dynamic_cast<message::anchor&>(msg));
		break;
	case message::message_type::proposal:
		handle(dynamic_cast<message::proposal&>(msg));
		break;
	}
}

void node::receive_message() {
	octet_string_view raw = _bus.receive();
	db::decoder dec(raw);
	std::unique_ptr<db::message::basic_message> msg;
	dec >> msg;
	handle(*msg);
}

void node::handle_election_timeout() {
	if (_mode == operating_mode::leader) {
		return;
	}

	if (_cfg.node_id != 1) {
		// Temporary measure for testing
		return;
	}

	_election_timeout.reset(_cfg.election_timeout_period);
	begin_new_term(_current_term + 1);

	_mode = operating_mode::candidate;
	_votes.insert(_cfg.node_id);
	_voted_for = _cfg.node_id;
	db::message::nomination msg(_current_term, _cfg.node_id,
		_log.last_log_index(), _log.last_log_term());
	db::encoder enc;
	enc << msg;
	_bus.send(enc.data());

	_cfg.logger->format(LOG_NOTICE,
		"Calling election for term {}",
		_current_term);
}

void node::send_heartbeat() {
	_heartbeat_timeout.reset(_cfg.heartbeat_timeout_period);

	std::vector<log_entry> entries;
	db::message::replication msg(_current_term, _cfg.node_id,
		_log.last_log_index(), _log.last_log_term(),
		std::move(entries), _commit_index);
	db::encoder enc;
	enc << msg;
	_bus.send(enc.data());
}

void node::replicate(index_type index) {
	_heartbeat_timeout.reset(_cfg.heartbeat_timeout_period);

	std::vector<log_entry> entries;
	entries.push_back(_log.at(index));
	db::message::replication msg(_current_term, _cfg.node_id,
		index - 1, _log.at(index - 1).term(),
		std::move(entries), _commit_index);
	db::encoder enc;
	enc << msg;
	_bus.send(enc.data());

	_cfg.logger->format(LOG_DEBUG,
		"Replicating log entry with index {} and term {}",
		index, _log.at(index).term());
}

void node::handle_heartbeat_timeout() {
	if (_mode == operating_mode::leader) {
		send_heartbeat();
	}
}

void node::update_commit_index() {
	std::vector<index_type> indexes;
	indexes.reserve(_followers.size());
	for (const auto& [node_id, fwr] : _followers) {
		// [TODO]: should distinguish between voting and non-voting nodes.
		indexes.push_back(fwr.match_index());

		_cfg.logger->format(LOG_DEBUG,
			"Considerating match index {} from follower {}",
			fwr.match_index(), node_id);
	}

	std::sort(indexes.begin(), indexes.end());

	// Adjust the majority threshold downwards by one since the leader
	// counts towards the number required, and is by definition fully
	// up-to-date, but is not included in the list of followers.
	ssize_t i = indexes.size() - (_cfg.quorum - 1);

	while (i >= 0) {
		index_type index = indexes[i];
		if (index < _commit_index) {
			// This is not an error condition: this node may have obtained
			// the value of its commit index from another node (before this
			// node became leader), and the other node might have had more
			// information about the log state of followers than this node
			// currently possesses.
			//
			// However, nor should this assessment of the commit index be
			// acted upon:
			//
			// - One of the functions of the commit index is to record
			//   which log entries have been applied to the state machine.
			// - Applying a log entry to the state machine is an
			//   irrevocable operation.
			// - It follows that the commit index should never be allowed
			//   to move backwards.
			break;
		}

		if (_log.at(index).term() == _current_term) {
			_commit_index = index;
			_cfg.logger->format(LOG_DEBUG,
				"Advanced commit index to {}",
				_commit_index);
			break;
		}
		i -= 1;
	}
	apply_committed_log_entries();
}

void node::apply_committed_log_entries() {
	index_type index = _state.last_applied();
	index_type first_index = index + 1;
	while (_commit_index > _state.last_applied()) {
		index += 1;
		_state.apply(_log.at(index));
	}
	if (index == first_index) {
		_cfg.logger->format(LOG_INFO,
			"Log entry {} has been committed", index);
	} else if (index > first_index){
		_cfg.logger->format(LOG_INFO,
			"Log entries {}-{} have been committed",
				first_index, index);
	}

	if (_state.last_applied() >=
		_snapshots.last_snapshot_index() + _cfg.snapshot_after) {

		data::any snapshot = _state.make_snapshot();
		_snapshots.write(snapshot.at("index"), snapshot);
	}
}

node::node(const config& cfg):
	_cfg(cfg),
	_log(_cfg.dirname / "log"),
	_snapshots(_cfg.dirname / "snapshot"),
	_server(*this, _cfg.sockpath),
	_server_event(_em, [&]{ accept_client_connection(); }, _server.fd()),
	_bus_event(_em, [&]{ receive_message(); }, _bus.fd()),
	_election_timeout(_em, [&]{ handle_election_timeout(); },
		_cfg.election_timeout_period),
	_heartbeat_timeout(_em, [&]{ handle_heartbeat_timeout(); },
		_cfg.heartbeat_timeout_period) {

	index_type index = _snapshots.last_snapshot_index();
	if (index != 0) {
		_cfg.logger->format(LOG_NOTICE,
			"Loading snapshot for index {}", index);
		_state.load_snapshot(_snapshots.read(index));
	}
}

void node::run() {
	while (true) {
		_em.poll();
	}
}

}
