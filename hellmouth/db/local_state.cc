// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/cbor/decoder.h"
#include "hellmouth/db/log_entry.h"
#include "hellmouth/db/local_state.h"

namespace hellmouth::db {

void local_state::load_snapshot(const data::any& snapshot) {
	_content = snapshot.at("content");
	_last_applied = snapshot.at("index");
}

data::any local_state::make_snapshot() const {
	std::lock_guard lock(_mutex);
	data::any encstate;
	encstate["content"] = _content;
	encstate["index"] = _last_applied;
	return encstate;
}

void local_state::apply(const log_entry& entry) {
	std::lock_guard lock(_mutex);
	bool complete = _assembler.add(entry.content());
	if (complete) {
		changeset cset = _assembler.assemble();
		_content.merge(cset.spec());

		auto sig = cset.signature();
		auto f = _notifications.find(sig);
		if (f != _notifications.end()) {
			f->second.notify(changeset_notification::success);
		}
	}
	_last_applied += 1;
}

changeset_notification& local_state::track(const crypto::ed25519::signature& sig) {
	std::lock_guard lock(_mutex);
	return _notifications[sig];
}

void local_state::untrack(const crypto::ed25519::signature& sig) {
	std::lock_guard lock(_mutex);
	_notifications.erase(sig);
}

}
