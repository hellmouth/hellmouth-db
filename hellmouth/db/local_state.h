// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_LOCAL_STATE
#define HELLMOUTH_DB_LOCAL_STATE

#include <cstdint>
#include <memory>
#include <map>
#include <mutex>

#include "hellmouth/data/fwd.h"
#include "hellmouth/data/any.h"
#include "hellmouth/crypto/ed25519/signature.h"
#include "hellmouth/db/types.h"
#include "hellmouth/db/changeset_assembler.h"
#include "hellmouth/db/changeset_notification.h"

namespace hellmouth::db {

class log_entry;

/** A class to represent the local state of the database.
 * Each database node maintains an instance of this class to record what
 * it considers to be the current state of the database, based upon the
 * messages it has observed. This may temporarily differ from the local
 * state of other nodes, due to propagation delays and packet loss.
 *
 * Note in particular that there is a difference between:
 *
 * - when a changeset is committed to the database (which typically happens
 *   when a majority of nodes have durably written the changeset to their
 *   logs, a state of affairs which is only be detectable with hindsight
 *   in non-trivial clusters); versus
 * - when a changeset is applied to the local state of a node (which
 *   happens only when that node becomes aware that the changeset has been
 *   committed).
 *
 * The public interface provided by this class is fully thread-safe.
 */
class local_state {
private:
	/** The current content. */
	data::any _content;

	/** The index of the last log entry that has been applied to the
	 * database content, or 0 if no log entries have been applied. */
	index_type _last_applied = 0;

	/** An object for assembling changesets from fragments. */
	changeset_assembler _assembler;

	/** Changesets requiring notification if and when applied. */
	std::map<crypto::ed25519::signature,
		changeset_notification> _notifications;

	/** A mutex to protect the other members of this class. */
	mutable std::mutex _mutex;
public:
	/** Construct local state for empty database. */
	local_state() = default;

	/** Load a snapshot.
	 * @param snapshot the snapshot to be loaded
	 */
	void load_snapshot(const data::any& snapshot);

	/** Make a snapshot of the local state.
	 * @return the resulting snapshot
	 */
	data::any make_snapshot() const;

	/** Get the current content of the database.
	 * The returned value is mutable, but it is a value not a reference,
	 * therefore:
	 *
	 * - any changes made to it will not be reflected in the database
	 * - any subsequent changesets applied to the database will not
	 *   be reflected in the value returned.
	 *
	 * @return the database content
	 */
	data::any content() const {
		std::lock_guard lock(_mutex);
		return _content;
	}

	/** Get the index of the most recently applied log entry.
	 * @return the index, or 0 if no log entries have been applied.
	 */
	index_type last_applied() const {
		return _last_applied;
	}

	/** Apply a log entry to the database.
	 * State changes are applied atomically.
	 *
	 * Defragmentation has not yet been implemented, but will be
	 * performed transparently by this function.
	 *
	 * @param entry the log entry to be applied
	 */
	void apply(const log_entry& entry);

	/** Track the progress of a changeset.
	 * @param sig the signature of the changeset to be tracked
	 * @return a changeset notification object for the given signature
	 */
	changeset_notification& track(const crypto::ed25519::signature& sig);

	/** Stop tracking the progress of a changeset.
	 * As currently implemented, track and untrack operations must
	 * be matched, and each changeset may only be tracked once.
	 */
	void untrack(const crypto::ed25519::signature& sig);
};

}

#endif
