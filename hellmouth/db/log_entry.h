// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_LOG_ENTRY
#define HELLMOUTH_DB_LOG_ENTRY

#include "hellmouth/db/types.h"
#include "hellmouth/db/changeset_fragment.h"

namespace hellmouth::db {

class encoder;
class decoder;

/** A class to represent a log entry. */
class log_entry {
private:
	/** The term when this log entry was received by the leader. */
	term_type _term = 0;

	/** The content of this log entry. */
	changeset_fragment _content;
public:
	/** Construct empty log entry. */
	log_entry() = default;

	/** Construct log entry.
	 * @param term the term when this log entry was received by the
	 *  leader
	 * @param content the content of this log entry
	 */
	log_entry(term_type term, changeset_fragment&& content):
		_term(term), _content(std::move(content)) {}

	/** Get the term when this log entry was received by the leader.
	 * @return the term number
	 */
	term_type term() const {
		return _term;
	}

	/** Get the encoded content of this log entry.
	 * @return the content
	 */
	const changeset_fragment& content() const {
		return _content;
	}
};

encoder& operator<<(encoder& enc, const log_entry& entry);
decoder& operator>>(decoder& dec, log_entry& entry);

}

#endif
