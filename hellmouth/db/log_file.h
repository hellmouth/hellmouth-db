// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_LOG_FILE
#define HELLMOUTH_DB_LOG_FILE

#include <stdexcept>
#include <vector>
#include <filesystem>

#include "hellmouth/os/file_descriptor.h"
#include "hellmouth/db/types.h"
#include "hellmouth/db/log_entry.h"

namespace hellmouth::db {

/** A class to represent a database log file.
 * All log indices passed in to and out of this class are now absolute.
 *
 * This class is not thread-safe, the expectation being that the class
 * db::log will mediate access.
 *
 * It is always permissible to store one entry in a log file, even if this
 * would cause the maximum size in bytes to be exceeded.
 *
 * [TODO]: it would be preferable to avoid having to read the whole file
 * when it is opened, and having to store the resulting offsets in memory.
 */
class log_file {
public:
	class log_file_full;
private:
	/** The pathname of this log file. */
	std::string _pathname;

	/** A file descriptor for accessing the log file. */
	mutable os::file_descriptor _fd;

	/** The index of the first entry in this log file. */
	index_type _base;

	/** Offsets for accessing the entries in this log file. */
	std::vector<size_t> _offsets;

	/* The maximum permitted size of this log file, in bytes. */
	size_t _max_psize;
public:
	/** Open database log file.
	 * If the file does not already exist then an empty log file is
	 * is created.
	 *
	 * @param dirname the pathname of the directory in which
	 *  log files are stored
	 * @param base the index of the first entry in this log file
	 * @param max_psize the maximum permitted size of this log file,
	 *  in bytes
	 */
	log_file(const std::filesystem::path& dirname, index_type base,
		size_t max_psize);

	/** Get the index of the first entry in this log file.
	 * @return the index
	 */
	index_type base() const {
		return _base;
	}

	/** Get the index of the first entry not in this log file.
	 * @return the index
	 */
	index_type limit() const {
		return _base + _offsets.size();
	}

	/** Get the physical size of this log file.
	 * @return the size, in bytes
	 */
	size_t psize() const;

	/** Get the log entry with a given index.
	 * @param index the required log entry index
	 * @return the corresponding log entry
	 */
	octet_string at(index_type index) const;

	/** Append an entry to this log file.
	 * @param entry the encoded log entry to be appended
	 * @throws std::length_error if log file is full
	 */
	void push_back(octet_string_view entry);

	/** Truncate log file at given index.
	 * @param index the index of the first log entry not preserved
	 */
	void truncate(index_type index);

	/** Unlink this log file.
	 * The file will continue to exist while it remains open,
	 * but will have been unlinked from the log file directory.
	 */
	void unlink();
};

class log_file::log_file_full:
	public std::runtime_error {
public:
	log_file_full():
		std::runtime_error("log file full") {}
};

}

#endif
