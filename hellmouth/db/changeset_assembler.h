// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_CHANGESET_ASSEMBLER
#define HELLMOUTH_DB_CHANGESET_ASSEMBLER

#include <vector>

#include "hellmouth/db/types.h"
#include "hellmouth/db/changeset.h"
#include "hellmouth/db/changeset_fragment.h"

namespace hellmouth::db {

/** A class for assembling changesets from fragments. */
class changeset_assembler {
private:
	/** The accumulated fragments. */
	std::vector<changeset_fragment> _fragments;
public:
	/** Add a fragment.
	 * @param fragment the fragment to be added
	 * @return true if the changeset is now complete,
	 *  otherwise false
	 */
	bool add(const changeset_fragment& fragment);

	/** Assemble the changeset.
	 * @return the resulting changeset
	 */
	changeset assemble();
};

}

#endif
