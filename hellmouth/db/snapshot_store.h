// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_SNAPSHOT_STORE
#define HELLMOUTH_DB_SNAPSHOT_STORE

#include <filesystem>

#include "hellmouth/data/any.h"
#include "hellmouth/db/types.h"

namespace hellmouth::db {

/** A class for the durable storage and retrieval of database snapshots.
 * Snapshots are labelled using the index of the most recent log entry
 * that had been incorporated into the database at that point.
 *
 * Snapshots are composed of structured data, represented using the class
 * data::any, but are otherwise opaque to this class.
 *
 * The behaviour of this class is undefined if instances referring to
 * the same pathname have overlapping lifetimes.
 */
class snapshot_store {
private:
	/** The pathname of the directory in which snapshot files
	 * are stored. */
	std::filesystem::path _dirname;

	/** The index of the most recent available snapshot,
	 * or 0 if none available */
	index_type _last_snapshot_index = 0;

	/** A mutex for preventing multiple snapshots from being written
	 * in parallel.
	 *
	 * The current implementation requires this for safety because
	 * otherwise a temporary file created by one thread could be made
	 * visible in partially-complete state by another thread.
	 * It also helps to limit the IO bandwidth which snapshotting
	 * can consume.
	 */
	std::mutex _mutex;

	/** Make the pathname of the snapshot file for a given index.
	 * @param index the snapshot index
	 * @param temporary true for a temporary file, otherwise false
	 * @return the corresponding pathname
	 */
	std::filesystem::path make_pathname(index_type index,
		bool temporary = false) const;
public:
	/** Construct database snapshot store.
	 * @param dirname the pathname of the directory in which
	 *  snapshot files are stored
	 */
	snapshot_store(const std::filesystem::path& dirname);

	/** Get the index of the most recent available snapshot.
	 * @return the snapshot index, or 0 if none available
	 */
	index_type last_snapshot_index() const {
		return _last_snapshot_index;
	}

	/** Read the content of a snapshot.
	 * @param index the snapshot index
	 * @return the corresponding snapshot content
	 */
	data::any read(index_type index) const;

	/** Write a snapshot.
	 * The snapshot is written atomically by first writing to a
	 * temporary filename in the same directory, then renaming
	 * once complete.
	 *
	 * If there is already a snapshot in progress (either for the
	 * same index or a different one) then this function will block
	 * until the existing snapshot has completed.
	 *
	 * @param index the snapshot index
	 * @param content the snapshot content to be written
	 */
	void write(index_type index, const data::any& content);
};

}

#endif
