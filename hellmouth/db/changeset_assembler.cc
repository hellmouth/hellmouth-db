// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/db/changeset_assembler.h"

namespace hellmouth::db {

bool changeset_assembler::add(const changeset_fragment& fragment) {
	if (!_fragments.empty()) {
		const changeset_fragment& back = _fragments.back();
		if (fragment.id() != back.id() + 1) {
			_fragments.clear();
		}
	}
	if (_fragments.empty()) {
		if (fragment.id() != 0) {
			return false;
		}
	}

	_fragments.push_back(fragment);
	return !fragment.more_fragments();
}

changeset changeset_assembler::assemble() {
	octet_string content;
	for (const auto& fragment : _fragments) {
		content.append(fragment.content());
	}
	_fragments.clear();
	return changeset(content);
}

}
