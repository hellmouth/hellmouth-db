// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include "hellmouth/db/encoder.h"
#include "hellmouth/db/decoder.h"
#include "hellmouth/db/changeset_fragment.h"

namespace hellmouth::db {

encoder& operator<<(encoder& enc, const changeset_fragment& frag) {
	if (frag.id() > 0x7fffffff) {
		throw std::invalid_argument("invalid fragment ID");
	}
	if (frag.content().length() > 0xffff) {
		throw std::invalid_argument("fragment too long");
	}

	uint32_t fragdata = frag.id() & 0x7fffffff;
	if (frag.more_fragments()) {
		fragdata |= 0x80000000;
	}
	uint16_t length = frag.content().length();

	enc << fragdata << length << frag.content();
	return enc;
}

decoder& operator>>(decoder& dec, changeset_fragment& frag) {
	uint32_t fragdata;
	uint16_t length;
	octet_string content;

	dec >> fragdata >> length;
	content.resize(length);
	dec >> content;

	uint32_t id = fragdata & 0x7fffffff;
	bool mf = fragdata & 0x80000000;
	frag = changeset_fragment(id, mf, content);
	return dec;
}

}
