// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_CHANGESET_FRAGMENT
#define HELLMOUTH_DB_CHANGESET_FRAGMENT

#include "hellmouth/db/types.h"

namespace hellmouth::db {

class encoder;
class decoder;

/** A class to represent a fragment of a changeset.
 * Changesets may be fragmented across multiple log entries, so that the
 * resulting messages are small enough for transmission over the message
 * bus as single Internet Protocol datagrams.
 *
 * To allow for reassembly, each fragment includes two pieces of metadata:
 * a fragment ID, and a more-fragments flag:
 *
 * - The fragment ID is used to number the fragments belonging to a given
 *   changeset, consecutively and upwards from zero.
 * - The more-fragments flag is set for all fragments belonging to a given
 *   changeset except for the last one.
 *
 * Each fragment contains part of the CBOR-encoded changeset, which can be
 * reconstructed by concatenating the relevant fragments in ID order.
 */
class changeset_fragment {
private:
	/** The fragment ID. */
	fragment_id_type _id = 0;

	/** The more-fragments flag. */
	bool _mf = false;

	/** The content of this fragment. */
	octet_string _content;
public:
	/** Construct empty changeset fragment. */
	changeset_fragment() = default;

	/** Construct changeset fragment.
	 * @param id the fragment ID
	 * @param mf the more-fragments flag
	 * @param content the content of this fragment
	 */
	changeset_fragment(fragment_id_type id, bool mf,
		octet_string_view content):
		_id(id), _mf(mf), _content(content) {}

	/** Get the fragment ID.
	 * @return the fragment ID
	 */
	fragment_id_type id() const {
		return _id;
	}

	/** Get the more-fragments flag.
	 * @return the more-fragments flag
	 */
	bool more_fragments() const {
		return _mf;
	}

	/** Get the content of this fragment.
	 * @return the content
	 */
	octet_string_view content() const {
		return _content;
	}
};

encoder& operator<<(encoder& enc, const changeset_fragment& frag);
decoder& operator>>(decoder& dec, changeset_fragment& frag);

}

#endif
