#// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_FOLLOWER
#define HELLMOUTH_DB_FOLLOWER

#include "hellmouth/db/types.h"

namespace hellmouth::db {

/** A class for collecting information about a follower. */
class follower {
private:
	/** Get the index of the last matching log entry present in
	 * the log of this node, and known to have been durably replicated
	 * to the node referred to.
	 */
	index_type _match_index = 0;
public:
	/** Get the index of the last matching log entry present in
	 * the log of this node, and known to have been durably replicated
	 * to the node referred to.
	 */
	index_type match_index() const {
		return _match_index;
	}

	/** Handle a matching log entry.
	 * Even in the absence of any network delay, a call to this
	 * function merely asserts that a matching log entry with the
	 * given index has been observed, and not that it is the latest
	 * such entry in the log. For this reason, the index is used to
	 * bound _match_index below but not above.
	 *
	 * @param index the index of the matching entry
	 */
	void handle_match(index_type index) {
		if (index > _match_index) {
			_match_index = index;
		}
	}
};

}

#endif
