// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_DECODER
#define HELLMOUTH_DB_DECODER

#include <cstdint>

#include "hellmouth/db/types.h"
#include "hellmouth/crypto/octet_buffer.h"

namespace hellmouth::db {

/** A class for decoding database cluster protocol messages. */
class decoder {
private:
	/** The encoded input. */
	octet_string_view _data;

	/** Ensure that a given number of octets remain available.
	 * @param count the number of octets required
	 */
	void _ensure(octet_string_view::size_type count) {
		if (_data.length() < count) {
			_end_of_data();
		}
	}

	/** Report that the end of the data was reached unexpectedly. */
	void _end_of_data();
public:
	/** Construct decoder.
	 * The octet string view passed into this function must remain
	 * valid until decoding has been completed.
	 *
	 * @param data the data to be decoded
	 */
	decoder(octet_string_view data):
		_data(data) {}

	/** Get the remaining data.
	 * @return the remaining data
	 */
	octet_string_view data() const {
		return _data;
	}

	/** Decode an 8-bit unsigned integer.
	 * @param value a buffer to hold the decoded value
	 */
	decoder& operator>>(uint8_t& value) {
		_ensure(1);
		value = _data[0];
		value <<= 8;
		value |= _data[1];
		_data.remove_prefix(2);
		return *this;
	}

	/** Decode a 16-bit unsigned integer.
	 * @param value a buffer to hold the decoded value
	 */
	decoder& operator>>(uint16_t& value) {
		_ensure(2);
		value = _data[0];
		value <<= 8;
		value |= _data[1];
		_data.remove_prefix(2);
		return *this;
	}

	/** Decode a 32-bit unsigned integer.
	 * @param value a buffer to hold the decoded value
	 */
	decoder& operator>>(uint32_t& value) {
		_ensure(4);
		value = _data[0];
		value <<= 8;
		value |= _data[1];
		value <<= 8;
		value |= _data[2];
		value <<= 8;
		value |= _data[3];
		_data.remove_prefix(4);
		return *this;
	}

	/** Decode a 64-bit unsigned integer.
	 * @param value a buffer to hold the decoded value
	 */
	decoder& operator>>(uint64_t& value) {
		_ensure(8);
		value = _data[0];
		value <<= 8;
		value |= _data[1];
		value <<= 8;
		value |= _data[2];
		value <<= 8;
		value |= _data[3];
		value <<= 8;
		value |= _data[4];
		value <<= 8;
		value |= _data[5];
		value <<= 8;
		value |= _data[6];
		value <<= 8;
		value |= _data[7];
		_data.remove_prefix(8);
		return *this;
	}

	/** Decode an octet string.
	 * If there is a length field then it must be decoded separately.
	 * The octet string provided as a buffer must be of the appropriate
	 * size for the required number of octets.

	 * @param value a buffer to hold the decoded value
	 */
	decoder& operator>>(octet_string& value) {
		_ensure(value.length());
		value = _data.substr(0, value.length());
		_data.remove_prefix(value.length());
		return *this;
	}

	/** Decode an octet buffer.
	 * If there is a length field then it must be decoded separately.
	 * Buffer must be of the appropriate size for the required number
	 * of octets.
	 *
	 * @param value a buffer to hold the decoded value
	 */
	decoder& operator>>(crypto::octet_buffer& value) {
		_ensure(value.size());
		_data.copy(&value[0], value.size());
		_data.remove_prefix(value.size());
		return *this;
	}
};

}

#endif
