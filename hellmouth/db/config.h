// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_CONFIG
#define HELLMOUTH_DB_CONFIG

#include <filesystem>

#include "hellmouth/log/logger.h"
#include "hellmouth/db/types.h"
#include "hellmouth/db/connection.h"

namespace hellmouth::db {

/** A structure for configuring database nodes.
 * This structure is intended for holding configuration parameters which:
 *
 * - Are not obtainable from either the persistent node state or the
 *   database itself; or alternatively
 * - Are obtainable from one of those locations, but there may be a need
 *   for that setting to be overridden.
 *
 * Note that some of the parameters currently provided would be more
 * appropriately located elsewhere once the necessary code has been
 * written, at which point they may be removed from this structure.
 */
struct config {
	/** The node ID. */
	node_id_type node_id = 1;

	/** The pathname of a directory for storing persistent data. */
	std::filesystem::path dirname = "/var/lib/hellmouth/db";

	/** The pathname of the UNIX domain socket used for communicating
	 * with database clients. */
	std::filesystem::path sockpath = connection::default_pathname;

	/** The minimum number of votes required to be elected leader.
	 * [TODO]: there should be a mechanism for calculating this
	 * automatically from the voting membership of the cluster,
	 * but the ability to override would likely remain useful.
	 */
	unsigned int quorum = 2;

	/** The time to wait before calling an election.
	 * [TODO]: this should be randomised, and shorter
	 */
	long election_timeout_period = 1000;

	/** The time to wait before sending a heartbeat.
	 * [TODO]: this should be shorter
	 */
	long heartbeat_timeout_period = 500;

	/** The maximum number of log entries between snapshots. */
	index_type snapshot_after = 1000;

	/** The audit log. */
	hellmouth::log::logger* logger = &hellmouth::log::default_logger();
};

}

#endif
