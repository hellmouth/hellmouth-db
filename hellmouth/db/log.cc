// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include "hellmouth/db/log.h"
#include "hellmouth/db/decoder.h"
#include "hellmouth/db/encoder.h"

namespace hellmouth::db {

log_entry log::_at(index_type index) const {
	octet_string encentry = _logfiles.at(index);
	db::decoder dec(encentry);
	log_entry entry;
	dec >> entry;
	return entry;
}

void log::_push_back(const log_entry& entry) {
	db::encoder enc;
	enc << entry;
	_logfiles.push_back(enc.data());
}

log::log(const std::filesystem::path& dirname, off_t max_psize):
	_logfiles(dirname),
	_max_psize(max_psize) {

	if (_logfiles.limit() == 0) {
		_push_back(log_entry());
	}
}

void log::begin_new_term(term_type) {
	_anchor_index = _logfiles.limit() - 1;
}

log_entry log::at(index_type index) const {
        std::lock_guard lock(_mutex);
	return _at(index);
}

index_type log::append(log_entry&& entry) {
        std::lock_guard lock(_mutex);
	_push_back(entry);
	return _logfiles.limit() - 1;
}

bool log::append(index_type anchor_index, term_type anchor_term,
	const std::vector<log_entry>& entries) {

        std::lock_guard lock(_mutex);
	if (anchor_index >= _logfiles.limit()) {
		return false;
	}
	if (_at(anchor_index).term() != anchor_term) {
		_anchor_index = anchor_index - 1;
		return false;
	}

	index_type index = anchor_index;
	for (const auto& entry : entries) {
		index += 1;
		if (index == _logfiles.limit()) {
			_push_back(entry);
		} else if (_at(index).term() != entry.term()) {
			_logfiles.truncate(index);
			_push_back(entry);
		}
	}
	_anchor_index = index;
	return true;
}

}
