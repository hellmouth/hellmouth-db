// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_LOG_FILE_SET
#define HELLMOUTH_DB_LOG_FILE_SET

#include <memory>
#include <queue>
#include <map>
#include <filesystem>

#include "hellmouth/db/types.h"
#include "hellmouth/db/log_file.h"

namespace hellmouth::db {

/** A class to represent the set of log files for a given database.
 * This class provides similar functionality to the class log_file,
 * presented via a similar API, but with the ability to persist the data
 * across multiple numbered log files instead of a single file.
 *
 * This in turn makes it possible for unwanted log entries to be deleted,
 * without any data being rewritten and without requiring that the log
 * size be fixed on creation, provided that deletion occurs in units of
 * files.
 *
 * [TODO]: there is currently no limit on the number of log files which
 * are kept open by this class, with consequences for file descriptor
 * usage, and memory consumed by offset tables.
 */
class log_file_set {
private:
	/** The pathname of the directory in which log file are stored. */
	std::filesystem::path _dirname;

	/** The maximum size of a log file, in bytes */
	size_t _max_psize;

	/** The maximum number of open log files. */
	unsigned int _max_open;

	/** The type of the map from log entry indices to log files. */
	typedef std::map<index_type, std::unique_ptr<log_file>> map_type;

	/** The map from log entry indices to log files.
	 * All log files have an entry in this table, whether or not
	 * they are currently open, however if not open then the
	 * value will be a null pointer.
	 *
	 * In the event that there are no log files, an entry is added
	 * corresponding to the default base index. This ensures that
	 * there is always at least one log file listed in this map,
	 * and that the log can therefore always be appended to.
	 * The corresponding file is not created immediately, but it will
	 * be created automatically when an attempt is made to access it.
	 */
	mutable map_type _files;

	/** A list of open log files, in order of least recent use.
	 * In this list, log files are identified by their first log entry
	 * index. The first entry in the list is the log file which was
	 * least recently used.
	 *
	 * The chosen representation (a linear list) would scale poorly
	 * if the number of open log files were large, however the
	 * expectatation is that it will be capped at a small number
	 * (in the range 2-5) and that a more scalable representation
	 * would be umlikely to deliver practical benefits.
	 */
	mutable std::vector<index_type> _open_files;

	/** Get open log file from iterator
	 * @param iter the iterator
	 * @return the corresponding open log file
	 */
	log_file& to_logfile(const map_type::iterator& iter) const;
public:
	/** Construct log file set.
	 * @param dirname the pathname of the directory in which log
	 *  files are stored
	 * @param default_base_index the base index to use if there
	 *  are no existing log files
	 * @param max_psize the maximum permitted size of a logfile,
	 *  in bytes
	 */
	log_file_set(const std::filesystem::path& dirname,
		index_type default_base_index = 0,
		size_t max_psize = 0x1000000,
		unsigned int max_open = 3);

	/** Get the index of the first entry in this log file set.
	 * @return the index
	 */
	index_type base() const {
		if (_files.empty()) {
			return 0;
		}
		return _files.begin()->first;
	}

	/** Get the index of the first entry not in this log file.
	 * @return the index
	 */
	index_type limit() const {
		if (_files.empty()) {
			return 0;
		}
		return to_logfile(std::prev(_files.end())).limit();
	}

	/** Get the log entry with a given index.
	 * @param index the required log entry index
	 * @return the corresponding log entry
	 */
	octet_string at(index_type index) const;

	/** Append an entry to this log file set.
	 * @param entry the encoded log entry to be appended
	 */
	void push_back(octet_string_view entry);

	/** Truncate log file set at given index.
	 * This will both delete any log files which should no longer
	 * exist, and truncate the most recent remaining log file if
	 * necessary.
	 *
	 * @param index the index of the first log entry not preserved
	 */
	void truncate(index_type index);
};

}

#endif
