#// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_CHANGESET
#define HELLMOUTH_DB_CHANGESET

#include <vector>

#include "hellmouth/db/types.h"
#include "hellmouth/data/any.h"
#include "hellmouth/crypto/ed25519/key_pair.h"
#include "hellmouth/crypto/ed25519/signature.h"

namespace hellmouth::db {

class changeset_fragment;

/** A class to represent a complete changeset.
 * The purpose of a changeset is to describe a proposed or confirmed change
 * to the database.
 *
 * [TODO]: a mechanism is needed for ensuring that changesets cannot be
 * re-used once they have been committed. This is expected to have the
 * side-effect of ensuring that signatures are unique, required so that
 * signatures can be used for tracking progress.
 */
class changeset {
private:
	/** The encoded changeset. */
	octet_string _enccset;
public:
	/** Construct changeset from specification.
	 * @param spec a specification for the required change to the
	 *  database
	 * @param keypair a keypair for signing the changeset
	 */
	changeset(const data::any& spec,
		const crypto::ed25519::key_pair& keypair);

	/** Construct signed changeset from encoded representation.
	 * The signature is validated during construction, but only to
	 * confirm that the changeset was signed with the specified public
	 * key. It does not follow that the owner of the public key is
	 * necessarily authorised to make the specified change to the
	 * database.
	 *
         * @param enccset the encoded changeset
         */
        explicit changeset(octet_string_view enccset);

	/** Get the specification.
	 * @return the specification
	 */
	data::any spec() const;

	/** Get the signature.
	 * @return the signature
	 */
	crypto::ed25519::signature signature() const;

	/** Break this changeset into fragments.
	 * @param max_size the maximum fragment content size
	 * @return the resulting fragments
	 */
	std::vector<changeset_fragment> fragments(size_t max_size) const;
};

}

#endif
