// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_ENCODER
#define HELLMOUTH_DB_ENCODER

#include <cstdint>

#include "hellmouth/db/types.h"

namespace hellmouth::db {

/** A class for encoding database cluster protocol messages. */
class encoder {
private:
	/** The encoded data. */
	octet_string _data;
public:
	/** Encode a 8-bit unsigned integer.
	 * @param value the value to be encoded
	 */
	encoder& operator<<(uint8_t value) {
		_data.push_back(value);
		return *this;
	}

	/** Encode a 16-bit unsigned integer.
	 * @param value the value to be encoded
	 */
	encoder& operator<<(uint16_t value) {
		_data.push_back(value >> 8);
		_data.push_back(value >> 0);
		return *this;
	}

	/** Encode a 32-bit unsigned integer.
	 * @param value the value to be encoded
	 */
	encoder& operator<<(uint32_t value) {
		_data.push_back(value >> 24);
		_data.push_back(value >> 16);
		_data.push_back(value >> 8);
		_data.push_back(value >> 0);
		return *this;
	}

	/** Encode a 64-bit unsigned integer.
	 * @param value the value to be encoded
	 */
	encoder& operator<<(uint64_t value) {
		_data.push_back(value >> 56);
		_data.push_back(value >> 48);
		_data.push_back(value >> 40);
		_data.push_back(value >> 32);
		_data.push_back(value >> 24);
		_data.push_back(value >> 16);
		_data.push_back(value >> 8);
		_data.push_back(value >> 0);
		return *this;
	}

	/** Encode a sequence of octets.
	 * If a length field is required, this must be encoded separately.
	 *
	 * @param value the value to be encoded
	 */
	encoder& operator<<(octet_string_view data) {
		_data.append(data);
		return *this;
	}

	/** Get the encoded output.
 	 * @return the encoded output
	 */
	octet_string_view data() const {
		return _data;
	}
};

}

#endif
