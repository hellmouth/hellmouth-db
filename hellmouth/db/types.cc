// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/db/types.h"

namespace hellmouth::db {

std::string to_string(operating_mode mode) {
	switch (mode) {
	case operating_mode::follower:
		return "follower";
	case operating_mode::candidate:
		return "candidate";
	case operating_mode::leader:
		return "leader";
	default:
		return "invalid";
	}
}

}
