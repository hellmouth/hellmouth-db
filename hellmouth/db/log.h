// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_LOG
#define HELLMOUTH_DB_LOG

#include <vector>
#include <mutex>
#include <filesystem>

#include "hellmouth/db/log_entry.h"
#include "hellmouth/db/log_file.h"
#include "hellmouth/db/log_file_set.h"

namespace hellmouth::db {

/** A class to represent the whole of the database log.
 * When fully implemented this class will provide a unified view of the
 * whole of the database log, providing the functionality required to
 * implement the Raft algorithm without regard to how the information is
 * stored.
 *
 * The current implementation stores all entries in a single log file,
 * and has no provision for snapshotting (so will grow monotonically).
 */
class log {
private:
	/** A mutex to protect the other members of this class. */
	mutable std::mutex _mutex;

	/** The set of log files in which the log is persisted. */
	log_file_set _logfiles;

	/** The index of the current requested log anchor. */
	index_type _anchor_index = 0;

	/** The maximum permitted size of a log file, in bytes.
	 * The log file is allowed to exceed this size if it would not
	 * otherwise be able to hold any log entries at all, however
	 * this is not expected to happen in normal use.
	 */
	off_t _max_psize;

	/** Get the log entry with a given index (no lock).
	 * @param index the required log entry index
	 * @return the corresponding log entry
	 */
	log_entry _at(index_type index) const;

	/** Append an entry to the log (without lock).
	 * @param entry the log entry to be appended
	 */
	void _push_back(const log_entry& entry);
public:
	/** Construct log.
	 * @param dirname the pathname of the directory in which
	 *  log files are stored
	 * @param max_psize the maximum permitted log file size, in bytes
	 */
	log(const std::filesystem::path& dirname,
		off_t max_psize = 0x1000000);

	/** Begin a new term.
	 * This function should be called by node::begin_new_term
	 * whenever it is itself called.
	 *
	 * @param term the new term number
	 */
	void begin_new_term(term_type term);

	/** Get the log entry with a given index.
	 * @param index the required log entry index
	 * @return the corresponding log entry
	 */
	log_entry at(index_type index) const;

	/** Unconditionally append an entry to this log.
	 * @param entry the entry to be appended
	 * @return the index of the log entry appended
	 */
	index_type append(log_entry&& entry);

	/** Conditionally append entries to this log.
	 * The entries are appended immediately following a given anchor
	 * point, but only if that anchor has been confirmed to be valid.
	 * This is the case if:
	 *
	 * - the log must contain an entry at the specified index, and
	 * - that log entry must match the specified term.
	 *
	 * If an entry is encountered which conflicts with existing content
	 * then the log is truncated at that point to resolve the conflict
	 * (since merely overwriting the existing content might violate the
	 * log matching property). Once this has happened, it is safe to
	 * append both the conflicting entry and any remaining entries
	 * without further checks.
	 *
	 * If any entries are encountered which match existing content then
	 * no action is taken in respect of those entries. Note in
	 * particular that this does not cause truncation.
	 *
	 * @param anchor_index the index of the log entry used as an anchor
	 * @param anchor_term the term which the anchor is expected to have
	 * @param entries the sequence of entries to be appended
	 * @return true if the anchor was valid, otherwise false
	 */
	bool append(index_type anchor_index, term_type anchor_term,
		const std::vector<log_entry>& entries);

	/** Get the index of the last entry in the log.
	 * @return the index, or 0 if the log is empty
	 */
	index_type last_log_index() const {
		return _logfiles.limit() - 1;
	}

	/** Get the term of the last entry in the log.
	 * @return the term, or 0 if the log is empty
	 */
	term_type last_log_term() const {
		return at(last_log_index()).term();
	}

	/** Get the index of the current requested log anchor.
	 * @return the index, or 0 if the log is empty
	 */
	index_type anchor_index() const {
		return _anchor_index;
	}

	/** Get the term of the current requested log anchor.
	 * @return the term, or 0 if the log is empty
	 */
	term_type anchor_term() const {
		return at(anchor_index()).term();
	}
};

}

#endif
