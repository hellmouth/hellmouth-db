// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_LOG_CACHE
#define HELLMOUTH_DB_LOG_CACHE

#include <map>

#include "hellmouth/db/types.h"
#include "hellmouth/db/message/replication.h"

namespace hellmouth::db {

class log;

/** A class for cacheing replication messages not usable immediately.
 * Replication messages are eligible for being cached if they are not
 * suitable for immediate incorporation into the log because either:
 *
 * - The specified anchor could not be checked due to there being no
 *   corresponding log entry; or
 * - The term number of the anchor was different from that of the
 *   corresponding log entry.
 *
 * Additionally, the term number of the leader responsible for sending the
 * message must have been current at the time the message was received
 * (but not necessarily when the message is acted upon).
 *
 * If the cache already contains a replication message with the same anchor
 * index then the newer message overwrites the older one. No attempt is made
 * to deduplicate messages with overlapping content.
 *
 * When messages are replayed from the cache they should be handled in the
 * same way as freshly-received log messages, except that they should never
 * overwrite the existing content of the log.
 *
 * Differences from vanilla Raft:
 * - The number of entries appended to the log in response to a replication
 *   message may exceed the number of entries sent.
 * - This could result in log entries being appended which are not present
 *   in the current leaders log, however this is acceptable because it
 *   should have been possible to reach the same log state by not missing
 *   previous replication messages.
 * - There is a need for the leader to be made aware of any additional log
 *   entries which were appended, because otherwise there could be a long
 *   delay between a log entry being committed and the leader distributing
 *   that information.
 */
class log_cache {
private:
	/** The cached replication messages, by anchor index. */
	std::map<index_type, message::replication> _messages;
public:
	/** Insert a replication message into the cache.
	 * @param msg the message to be inserted
	 */
	void insert(message::replication&& msg);

	/** Replay cached messages into the log. */
	void replay(db::log& log);
};

}

#endif
