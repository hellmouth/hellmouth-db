// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/db/changeset_notification.h"

namespace hellmouth::db {

void changeset_notification::notify(outcome_type outcome) {
	std::lock_guard<std::mutex> lock(_mutex);
	_outcome = outcome;
	_cond.notify_one();
}

changeset_notification::outcome_type changeset_notification::await() {
	std::unique_lock lock(_mutex);
	_cond.wait(lock, [this]{ return _outcome != outcome_type::pending; });
	return _outcome;
}

}
