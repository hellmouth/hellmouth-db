// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <iterator>
#include <algorithm>
#include <regex>

#include "hellmouth/db/log_file_set.h"

namespace hellmouth::db {

static const std::regex filename_pattern(R"([0-9a-f]{16}[.]log)");

log_file& log_file_set::to_logfile(const map_type::iterator& iter) const {
	auto& [file_index, file_ptr] = *iter;

	if (file_ptr) {
		// If the required log file is already open then temporarily
		// remove it from the list of open files, in preparation for
		// later re-insertion at the end of the list.
		auto f = find(_open_files.begin(), _open_files.end(),
			file_index);
		if (f != _open_files.end()) {
			_open_files.erase(f);
		}
	} else {
		// If the limit of open files has been reached then select
		// the least recently-used file for closure, updating both
		// the log file map and the open log file list accordingly.
		if (_open_files.size() >= _max_open && !_open_files.empty()) {
			_files.at(_open_files.front()).reset();
			_open_files.erase(_open_files.begin());
		}

		// Open the requested log file.
		file_ptr = std::make_unique<log_file>(
			_dirname, file_index, _max_psize);
	}

	// Regardless or whether the file was previously open or not,
	// it should now appear at the end of the list of open log files.
	_open_files.push_back(file_index);
	return *file_ptr;
}

log_file_set::log_file_set(const std::filesystem::path& dirname,
	index_type default_base_index,
	size_t max_psize, unsigned int max_open):
	_dirname(dirname),
	_max_psize(max_psize),
	_max_open(max_open) {

	for (const auto& entry: std::filesystem::directory_iterator(dirname)) {
		std::string pathname = entry.path().filename().string();
		if (regex_match(pathname, filename_pattern)) {
			std::string hexindex(pathname, 0, 16);
			index_type index = strtoull(hexindex.c_str(), 0, 16);
			_files[index] = std::unique_ptr<log_file>();
		}
	}

	if (_files.empty()) {
		_files[default_base_index] = std::unique_ptr<log_file>();
	}
}

octet_string log_file_set::at(index_type index) const {
	auto f = _files.upper_bound(index);
	if (f == _files.begin()) {
		throw std::out_of_range("no log file for index");
	}
	return to_logfile(std::prev(f)).at(index);
}

void log_file_set::push_back(octet_string_view entry) {
	try {
		to_logfile(std::prev(_files.end())).push_back(entry);
	} catch (log_file::log_file_full&) {
		_files[limit()] = std::unique_ptr<log_file>();
		to_logfile(std::prev(_files.end())).push_back(entry);
	}
}

void log_file_set::truncate(index_type index) {
	if (index < base()) {
		throw std::out_of_range("log index too low for truncation");
	}
	if (index > limit()) {
		throw std::out_of_range("log index too high for truncation");
	}
	while (index < std::prev(_files.end())->first) {
		auto f = std::prev(_files.end());
		to_logfile(f).unlink();
		_files.erase(f);
	}
	to_logfile(std::prev(_files.end())).truncate(index);
}

}
