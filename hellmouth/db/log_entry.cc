// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/db/encoder.h"
#include "hellmouth/db/decoder.h"
#include "hellmouth/db/log_entry.h"

namespace hellmouth::db {

encoder& operator<<(encoder& enc, const log_entry& entry) {
	enc << entry.term() << entry.content();
	return enc;
}

decoder& operator>>(decoder& dec, log_entry& entry) {
	term_type term;
	changeset_fragment content;
	dec >> term >> content;
	entry = log_entry(term, std::move(content));
	return dec;
}

}
