#// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_CHANGESET_NOTIFICATION
#define HELLMOUTH_DB_CHANGESET_NOTIFICATION

#include <mutex>
#include <condition_variable>

namespace hellmouth::db {

/** A class for obtaining notification that a given proposal has been
 * committed. */
class changeset_notification {
public:
	/** An enumeration class for indicating the outcome of a proposal. */
	enum outcome_type {
		/** Indicate that a proposal remains pending. */
		pending,

		/** Indicate that a proposal was successfully committed. */
		success,

		/** Indicate that a proposal failed. */
		failure
	};
private:
	/** A mutex for use in conjunction with the condition variable. */
	std::mutex _mutex;

	/** A condition variable for notifying the client thread. */
	std::condition_variable _cond;

	/** The outcome of the proposal, or pending if no outcome yet. */
	outcome_type _outcome = pending;
public:
	/** Notify the client thread of the outcome.
	 * @param outcome the outcome of the proposal
	 */
	void notify(outcome_type outcome);

	/** Await the outcome of the proposal.
	 * This function should be called by the client thread, after
	 * ensuring that this notification object has been registered with
	 * the local state.
	 *
	 * @return the outcome of the proposal
	 */
	outcome_type await();
};

}

#endif
