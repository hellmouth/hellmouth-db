// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/db/log.h"
#include "hellmouth/db/log_cache.h"

namespace hellmouth::db {

void log_cache::insert(message::replication&& msg) {
	_messages.insert_or_assign(msg.anchor_index(), std::move(msg));
}

void log_cache::replay(db::log& log) {
	auto f = _messages.find(log.last_log_index());
	while (f != _messages.end()) {
		const auto& msg = f->second;
		bool match = log.append(
			msg.anchor_index(), msg.anchor_term(), msg.entries());
		if (!match) {
			break;
		}
		_messages.erase(_messages.begin(), std::next(f));
	}
}

}
