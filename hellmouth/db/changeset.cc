// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <sstream>

#include "hellmouth/cbor/encoder.h"
#include "hellmouth/cbor/decoder.h"
#include "hellmouth/db/encoder.h"
#include "hellmouth/db/decoder.h"
#include "hellmouth/db/changeset.h"
#include "hellmouth/db/changeset_fragment.h"

namespace hellmouth::db {

namespace  ed25519 = crypto::ed25519;

changeset::changeset(const data::any& spec,
	const ed25519::key_pair& keypair) {

	std::ostringstream ss;
	cbor::encoder cborenc(ss);
	cborenc(spec);
	std::string specstr = ss.str();
	octet_string encspec(
		(unsigned char*)specstr.data(), specstr.length());

	db::encoder covenc;
	covenc << encspec;
	octet_string_view covered = covenc.data();
	auto sig = keypair.private_key().sign(covered.data(), covered.size());

	db::encoder csetenc;
	csetenc << keypair.public_key() << sig << covered;
	_enccset = csetenc.data();
}

changeset::changeset(octet_string_view enccset):
	_enccset(enccset) {

	db::decoder dec(_enccset);
	ed25519::public_key pubkey;
	ed25519::signature sig;
	dec >> pubkey >> sig;
	octet_string_view covered = dec.data();
	pubkey.verify(sig, covered.data(), covered.size());
}

data::any changeset::spec() const {
	db::decoder dec(_enccset);
	ed25519::public_key pubkey;
	ed25519::signature sig;
	dec >> pubkey >> sig;
	octet_string_view covered = dec.data();

	std::string specstr((char*)covered.data(), covered.size());
	std::istringstream ss(specstr);
	cbor::decoder decode(ss);
	return decode();
}

crypto::ed25519::signature changeset::signature() const {
	db::decoder dec(_enccset);
	ed25519::public_key pubkey;
	ed25519::signature sig;
	dec >> pubkey >> sig;
	return sig;
}

std::vector<changeset_fragment> changeset::fragments(size_t max_size) const {
	std::vector<changeset_fragment> fragments;
	octet_string_view remaining = _enccset;
	fragment_id_type fragment_id = 0;
	while (!remaining.empty()) {
		size_t count = remaining.length();
		bool mf = false;
		if (count > max_size) {
			count = max_size;
			mf = true;
		}
		fragments.emplace_back(fragment_id++, mf,
			remaining.substr(0, count));
		remaining.remove_prefix(count);
	}
	return fragments;
}

}
