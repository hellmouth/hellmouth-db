// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_TYPES
#define HELLMOUTH_DB_TYPES

#include <cstdint>
#include <string>
#include <string_view>

namespace hellmouth::db {

/** A container class for holding a sequence of octets. */
typedef std::basic_string<unsigned char> octet_string;

/** A class for referring to a sequence of octets. */
typedef std::basic_string_view<unsigned char> octet_string_view;

/** A type for identifying cluster nodes. */
typedef uint16_t node_id_type;

/** A type for indexing log entries.
 * The first substantive log entry has an index of 1.
 * A vaule of 0 acts as a placeholder, notionally referring to the
 * predecessor of the first log entry.
 */
typedef uint64_t index_type;

/** A type for numbering terms.
 * The first substantive term has a number of 1.
 * A value of 0 acts as a placeholder, notionally referring to the
 * predecessor of the first term.
 */
typedef uint64_t term_type;

/** A type for numbering changeset fragments.
 * The first fragment has an ID of 0.
 */
typedef uint32_t fragment_id_type;

enum class operating_mode {
	follower,
	candidate,
	leader
};

/** Convert operating mode to string.
 * @param mode an operating mode
 * @return the corresponding string
 */
std::string to_string(operating_mode mode);

}

#endif
