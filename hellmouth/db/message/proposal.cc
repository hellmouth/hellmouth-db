// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <sstream>

#include "hellmouth/db/encoder.h"
#include "hellmouth/db/decoder.h"
#include "hellmouth/db/message/proposal.h"

namespace hellmouth::db::message {

message_type proposal::type() const {
	return message_type::proposal;
}

proposal::operator std::string() const {
	std::ostringstream out;
	out << "proposal("
		<< term() << ','
		<< proposer_id() << ','
		<< _fragment.content().length() << ')';
	return out.str();
}

encoder& proposal::encode(encoder& enc) const {
	basic_message::encode(enc);
	enc << _fragment;
	return enc;
}

proposal::proposal(decoder& dec):
	basic_message(dec) {

	dec >> _fragment;
}

proposal::proposal(term_type term, node_id_type proposer_id,
	changeset_fragment&& fragment):
	basic_message(term, proposer_id),
	_fragment(std::move(fragment)) {}

}
