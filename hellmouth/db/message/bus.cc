// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/cbor/encoder.h"
#include "hellmouth/cbor/decoder.h"
#include "hellmouth/db/message/bus.h"

namespace hellmouth::db::message {

const net::inet4::address bus::default_address("239.255.0.0");
const uint16_t bus::default_port = 666;

bus::bus(const net::inet4::address& mcaddr, uint16_t port):
	_srcaddr(0),
	_dstaddr(0),
	_fd(AF_INET, SOCK_DGRAM, 0),
	_buffer(0x10000) {

	_srcaddr.sin_family = AF_INET;
	_srcaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	_srcaddr.sin_port = htons(port);

	_dstaddr.sin_family = AF_INET;
	_dstaddr.sin_addr = mcaddr;
	_dstaddr.sin_port = htons(port);

	int reuseaddr = 1;
	_fd.setsockopt(SOL_SOCKET, SO_REUSEADDR, reuseaddr);
	_fd.bind(_srcaddr);
	_fd.add_membership(_dstaddr.sin_addr);
}

void bus::send(octet_string_view msg) {
	_fd.sendto(msg.data(), msg.length(), 0, _dstaddr);
}

bus::octet_string_view bus::receive() {
	size_t count = _fd.recv(&_buffer[0], _buffer.size());
	return octet_string_view(&_buffer[0], count);
}

}
