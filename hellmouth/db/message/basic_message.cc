// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/db/message/basic_message.h"
#include "hellmouth/db/message/nomination.h"
#include "hellmouth/db/message/vote.h"
#include "hellmouth/db/message/replication.h"
#include "hellmouth/db/message/anchor.h"
#include "hellmouth/db/message/proposal.h"

namespace hellmouth::db::message {

encoder& basic_message::encode(encoder& enc) const {
	enc << uint16_t(type()) << _term << _sender_id;
	return enc;
}

basic_message::basic_message(decoder& dec) {
	uint16_t numtype;
	dec >> numtype >> _term >> _sender_id;
}

basic_message::basic_message(term_type term, node_id_type sender_id):
	_term(term), _sender_id(sender_id) {}

decoder& operator>>(decoder& dec, std::unique_ptr<basic_message>& msg) {
	decoder preview(dec);
	uint16_t numtype;
	preview >> numtype;
	switch (message_type(numtype)) {
	case message_type::nomination:
		msg = std::make_unique<nomination>(dec);
		break;
	case message_type::vote:
		msg = std::make_unique<vote>(dec);
		break;
	case message_type::replication:
		msg = std::make_unique<replication>(dec);
		break;
	case message_type::anchor:
		msg = std::make_unique<anchor>(dec);
		break;
	case message_type::proposal:
		msg = std::make_unique<proposal>(dec);
		break;
	default:
		throw std::runtime_error("unrecognised message type");
	}
	return dec;
}

}
