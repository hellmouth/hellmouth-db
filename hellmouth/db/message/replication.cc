// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <sstream>

#include "hellmouth/db/encoder.h"
#include "hellmouth/db/decoder.h"
#include "hellmouth/db/message/replication.h"

namespace hellmouth::db::message {

message_type replication::type() const {
	return message_type::replication;
}

replication::operator std::string() const {
	std::ostringstream out;
	out << "replication("
		<< term() << ','
		<< leader_id() << ','
		<< _anchor_index << ','
		<< _anchor_term << ",[";
	bool first = true;
	for (const auto& entry : _entries) {
		if (first) {
			first = false;
		} else {
			out << ',';
		}
		out << entry.content().content().length();
	}
	out << "]," << _leader_commit << ')';
	return out.str();
}

encoder& replication::encode(encoder& enc) const {
	basic_message::encode(enc);
	enc << _anchor_index << _anchor_term;
	enc << uint16_t(_entries.size());
	for (const auto& entry : _entries) {
		enc << entry;
	}
	enc << _leader_commit;
	return enc;
}

replication::replication(decoder& dec):
	basic_message(dec) {

	dec >> _anchor_index >> _anchor_term;
	uint16_t entry_count;
	dec >> entry_count;
	_entries.resize(entry_count);
	for (uint16_t i = 0; i != entry_count; ++i) {
		dec >> _entries[i];
	}
	dec >> _leader_commit;
}

replication::replication(term_type term, node_id_type leader_id,
	index_type anchor_index, term_type anchor_term,
	std::vector<log_entry>&& entries, index_type leader_commit):
	basic_message(term, leader_id),
	_anchor_index(anchor_index), _anchor_term(anchor_term),
	_entries(std::move(entries)), _leader_commit(leader_commit) {}

}
