// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_MESSAGE_BASIC_MESSAGE
#define HELLMOUTH_DB_MESSAGE_BASIC_MESSAGE

#include <memory>
#include <string>

#include "hellmouth/db/types.h"
#include "hellmouth/db/encoder.h"
#include "hellmouth/db/decoder.h"

namespace hellmouth::db::message {

enum class message_type {
	nomination = 0,
	vote = 1,
	replication = 2,
	anchor = 3,
	proposal = 4
};

/** A base class to represent a database protocol message.
 * On receipt of any message, if the term specified in the message is
 * greater than the recipient's current term then:
 *
 * - The recipient's current term is updated.
 * - The recipient becomes a follower, if it was not one already.
 */
class basic_message {
private:
	/** The current term of the sender. */
	term_type _term;

	/** The node ID of the sender. */
	node_id_type _sender_id;
public:
	/** Decode basic_message.
	 * @param dec a decoder for interpreting the message content
	 */
	basic_message(db::decoder& dec);

	/** Construct basic_message.
	 * @param term the current term of the sender
	 * @param sender_id the node ID of the sender
	 */
	basic_message(term_type term, node_id_type sender_id);

	/** Get the current term of the sender. */
	term_type term() const {
		return _term;
	}

	/** Get the node ID of the sender. */
	node_id_type sender_id() const {
		return _sender_id;
	}

	/** Get the type of this message. */
	virtual message_type type() const = 0;

	/** Convert this message into human-readable form.
	 * @return a human-readable representation of this message
	 */
	virtual operator std::string() const = 0;

	/** Encode this message.
	 * @enc an encoder for building the message content
	 * @return the encoder
	 */
	virtual encoder& encode(encoder& enc) const;
};

inline encoder& operator<<(encoder& enc, const basic_message& msg) {
	msg.encode(enc);
	return enc;
}

decoder& operator>>(decoder& dec, std::unique_ptr<basic_message>& msg);

}

#endif
