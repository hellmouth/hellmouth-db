// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_MESSAGE_NOMINATION
#define HELLMOUTH_DB_MESSAGE_NOMINATION

#include "hellmouth/db/types.h"
#include "hellmouth/db/message/basic_message.h"

namespace hellmouth::db::message {

/** A message class for nominating a node as a candidate to become leader.
 * A node should nominate itself to become leader if:
 *
 * - the cluster appears to be without a current leader;
 * - the node is eligible to stand.
 *
 * However, this should only happen on the expiry of an election timer,
 * the period of which is randomised to reduce the likelihood of split
 * votes.
 *
 * The cluster is considered to have a current leader if a recent log
 * replication message has been observed with a non-historical term
 * number, or if the node in question is itself the leader.
 *
 * A node is eligible to stand if it is a current voting member of the
 * cluster.
 *
 * Nodes should only nominate themselves, as opposed to other nodes. For
 * this reason, the candidate ID for this message cannot be specified
 * independently of the sender ID.
 *
 * The candidate should increment its term immediately prior to sending
 * this message, so that it is higher than the term of any other message
 * previously observed. One effect of this action is that the node does
 * not need to consider whether it has previously voted for a candidate
 * during the new term.
 *
 * Differences from vanilla Raft:
 *
 * - Because nominations are multicast, they may be seen by nodes which are
 *   not eligible to vote.
 * - Current practice is for this message to be sent once only, with no
 *   provision for retries during the same term. If either the nomination
 *   or votes are lost, a new election will be called by either this or
 *   another node.
 */
class nomination:
	public basic_message {
private:
	/** The index of the candidate's last log entry. */
	index_type _last_log_index;

	/** The term of the candidate's last log entry. */
	term_type _last_log_term;
public:
	message_type type() const override;
	operator std::string() const override;
	encoder& encode(encoder& enc) const override;

	/** Decode nomination message.
	 * @param dec a decoder for interpreting the message content
	 */
	nomination(decoder& dec);

	/** Construct nomination message.
	 * @param term the candidate's term
	 * @param sender_id the node ID of the sender/candidate
	 * @param last_log_index the index of the candidate's last log entry
	 * @param last_log_term the term of the candidate's last log entry
	 */
	nomination(term_type term, node_id_type sender_id,
		index_type last_log_index, term_type last_log_term);

	/** Get the node ID of the candidate.
	 * @return the node ID
	 */
	node_id_type candidate_id() const {
		return sender_id();
	}

	/** Get the index of the candidate's last log entry.
	 * @return the log entry index
	 */
	index_type last_log_index() const {
		return _last_log_index;
	}

	/** Get the term of the candidate's last log entry.
	 * @return the log entry term
	 */
	term_type last_log_term() const {
		return _last_log_term;
	}
};

}

#endif
