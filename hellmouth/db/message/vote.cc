// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <sstream>

#include "hellmouth/db/encoder.h"
#include "hellmouth/db/decoder.h"
#include "hellmouth/db/message/vote.h"

namespace hellmouth::db::message {

message_type vote::type() const {
	return message_type::vote;
}

vote::operator std::string() const {
	std::ostringstream out;
	out << "vote("
		<< term() << ','
		<< sender_id() << ','
		<< candidate_id() << ')';
        return out.str();
}

encoder& vote::encode(encoder& enc) const {
	basic_message::encode(enc);
	enc << _candidate_id;
	return enc;
}

vote::vote(decoder& dec):
	basic_message(dec) {

	dec >> _candidate_id;
}

vote::vote(term_type term, node_id_type sender_id,
	node_id_type candidate_id):
	basic_message(term, sender_id), _candidate_id(candidate_id) {}
}
