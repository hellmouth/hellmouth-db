// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_MESSAGE_FWD
#define HELLMOUTH_DB_MESSAGE_FWD

namespace hellmouth::db::message {

class basic_message;
class nomination;
class vote;
class replication;
class anchor;
class proposal;

}

#endif
