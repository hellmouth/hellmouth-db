// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_MESSAGE_BUS
#define HELLMOUTH_DB_MESSAGE_BUS

#include <string_view>
#include <vector>

#include <sys/socket.h>

#include "hellmouth/os/socket_descriptor.h"
#include "hellmouth/net/inet4/address.h"

namespace hellmouth::db::message {

/** A class to represent the message bus of a database cluster. */
class bus {
public:
	/** A type to represent a sequence of octets. */
	typedef std::basic_string_view<unsigned char> octet_string_view;
private:
	/** The default multicast address. */
	static const net::inet4::address default_address;

	/** The default port number. */
	static const uint16_t default_port;

	/** The source socket address. */
	struct sockaddr_in _srcaddr;

	/** The destination socket address. */
	struct sockaddr_in _dstaddr;

	/** A socket descriptor for communicating with other members
	 * of the cluster. */
	os::socket_descriptor _fd;

	/** A buffer for receiving datagrams. */
	std::vector<unsigned char> _buffer;
public:
	/** Construct message bus.
	 * @param mcaddr the required multicast address
	 * @param port the required port number
	 */
	bus(const net::inet4::address& mcaddr = default_address,
		uint16_t port = default_port);

	/** Get the socket descriptor.
	 * @return the socket descriptor
	 */
	const os::socket_descriptor& fd() const {
		return _fd;
	}

	/** Send a message.
	 * @param msg the message to be sent
	 */
	void send(octet_string_view msg);

	/** Receive a message.
	 * The returned data will remain valid until the next call to this
	 * function, or the message bus object is destroyed.
	 *
	 * @return the message received
	 */
	octet_string_view receive();
};

}

#endif
