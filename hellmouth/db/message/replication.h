// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_MESSAGE_REPLICATION
#define HELLMOUTH_DB_MESSAGE_REPLICATION

#include <vector>

#include "hellmouth/db/types.h"
#include "hellmouth/db/log_entry.h"
#include "hellmouth/db/message/basic_message.h"

namespace hellmouth::db::message {

/** A message class for replicating log entries.
 * Replication messages may only be sent by the current leader. Each
 * message contains a sequence of zero or more log entries from the
 * leader's log, together with the index and term of an anchor point
 * for ensuring that the log matching property is maintained.
 *
 * Provided that the anchor is valid, followers should append any log
 * entry sequences they receive to their local log, truncating the log
 * if necessary to resolve conflicts.
 *
 * It is permissible for the leader to send any sequence of entries from
 * its log at any time, however there are obvious benefits if it can limit
 * the entries sent to those which can be usefully acted upon.
 *
 * Replication messages with an empty log entry sequence are used as a
 * heartbeat to maintain the authority of the leader if there are no
 * log entries in need of replication.
 *
 * Differences from vanilla Raft:
 *
 * - All nodes may participate in log replication, whether or not they are
 *   eligible to vote.
 * - Because replication messages are multicast, nodes may encounter log
 *   entries which were not sent for their benefit.
 * - When deciding which log entries to replicate and in what order,
 *   leaders will achieve better results if they take a holistic view of
 *   the log status of all nodes (however there is a risk of degrading
 *   latency if this is taken too far).
 * - The leader is not expected to replicate log entries for the benefit
 *   of unresponsive nodes. Doing so would rarely serve any useful purpose,
 *   and once the recovering node sees a heartbeat or other replication
 *   message it will be able to re-establish its status as a responsive
 *   node by sending an anchor message.
 * - The mechanism for maintaining the log matching property has been
 *   reframed using the concept of an anchor point, however there is no
 *   substantive change to its method of operation.
 */
class replication:
	public basic_message {
private:
	/** The index of the log entry to which the sequence should be
	 * appened. */
	index_type _anchor_index;

	/** The term of the log entry to which the sequence should be
	 * appended. */
	term_type _anchor_term;

	/** The log entries to be appended. */
	std::vector<log_entry> _entries;

	/** The index of the last log entry known by the leader
	 * to have been committed by the cluster. */
	index_type _leader_commit;
public:
	message_type type() const override;
	operator std::string() const override;
	encoder& encode(encoder& enc) const override;

	/** Decode log replication message.
	 * @param dec a decoder for interpreting the message content
	 */
	replication(decoder& dec);

	/** Construct log replication message.
	 * @param term the leader's term
	 * @param leader_id the node ID of the leader
	 * @param anchor_index the index of the log entry from which the
	 *  sequence is anchored
	 * @param anchor_term the term of the log entry from which the
	 *  sequence is anchored
	 * @param entries the log entries to be appended
	 * @param leader_commit the index of the last log entry known by
	 *  the leader to have been committed by the cluster
	 */
	replication(term_type term, node_id_type leader_id,
		index_type anchor_index, term_type anchor_term,
		std::vector<log_entry>&& entries, index_type leader_commit);

	/** Get the node ID of the leader.
	 * This is the same as the node ID of the sender.
	 *
	 * @return the node ID
	 */
	node_id_type leader_id() const {
		return sender_id();
	}

	/** Get the index of the log entry to which the sequence should
	 * be appened.
	 * @return the index
	 */
	index_type anchor_index() const {
		return _anchor_index;
	}

	/** Get the term of the log entry to which the sequence should
	 * be appened.
	 * @return the term
	 */
	term_type anchor_term() const {
		return _anchor_term;
	}

	/** Get the sequence of log entries to be appended.
	 * @return the log entries
	 */
	const std::vector<log_entry>& entries() const {
		return _entries;
	}

	/** Get the index of the last log entry known by the leader
	 * to have been committed by the cluster.
	 *
	 * @return the log index
	 */
	index_type leader_commit() const {
		return _leader_commit;
	}
};

}

#endif
