// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <sstream>

#include "hellmouth/db/encoder.h"
#include "hellmouth/db/decoder.h"
#include "hellmouth/db/message/nomination.h"

namespace hellmouth::db::message {

message_type nomination::type() const {
	return message_type::nomination;
}

nomination::operator std::string() const {
	std::ostringstream out;
	out << "nomination("
		<< term() << ','
		<< sender_id() << ','
		<< _last_log_index << ','
		<< _last_log_term << ')';
        return out.str();
}

encoder& nomination::encode(encoder& enc) const {
	basic_message::encode(enc);
	enc << _last_log_index << _last_log_term;
	return enc;
}

nomination::nomination(decoder& dec):
	basic_message(dec) {

	dec >> _last_log_index >> _last_log_term;
}

nomination::nomination(term_type term, node_id_type sender_id,
	index_type last_log_index, term_type last_log_term):
	basic_message(term, sender_id),
	_last_log_index(last_log_index), _last_log_term(last_log_term) {}

}
