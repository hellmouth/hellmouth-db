// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_MESSAGE_PROPOSAL
#define HELLMOUTH_DB_MESSAGE_PROPOSAL

#include "hellmouth/db/types.h"
#include "hellmouth/db/changeset_fragment.h"
#include "hellmouth/db/message/basic_message.h"

namespace hellmouth::db::message {

/** A message class for proposing part or all of changeset. */
class proposal:
	public basic_message {
private:
	/** A fragment of the proposed changeset. */
	changeset_fragment _fragment;
public:
	message_type type() const override;
	operator std::string() const override;
	encoder& encode(encoder& enc) const override;

	/** Decode proposal message.
	 * @param dec a decoder for interpreting the message content
	 */
	proposal(decoder& dec);

        /** Construct proposal message.
         * @param term the leader's term
         * @param proposer_id the node ID of the proposer
	 * @param fragment the changeset fragment
         */
        proposal(term_type term, node_id_type proposer_id,
		changeset_fragment&& fragment);

	/** Get the node ID of the proposer.
	 * This is the same as the node ID of the sender.
	 *
	 * @return the node ID
	 */
	node_id_type proposer_id() const {
		return sender_id();
	}

	/** Get the fragment of the proposed changeset.
	 * @return the changeset fragment
	 */
	const changeset_fragment& fragment() const {
		return _fragment;
	}
};

}

#endif
