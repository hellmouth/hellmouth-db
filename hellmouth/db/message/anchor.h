// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_MESSAGE_ANCHOR
#define HELLMOUTH_DB_MESSAGE_ANCHOR

#include "hellmouth/db/types.h"
#include "hellmouth/db/message/basic_message.h"

namespace hellmouth::db::message {

/** A message class for identifying potential log replication anchors.
 * When log entries are replicated they must be anchored at either the
 * immediately preceding log entry or the start of the log, in order to
 * ensure that the log matching property will not be violated by the
 * outcome. Anchors are identified by means of their index and term.
 *
 * The ostensible function of an anchor message is merely to communicate
 * the fact that a given index and term could be used as an anchor. As such
 * it may be sent by any node at any time, and refer to any log entry that
 * was durably present in the log of that node at the time. However, its
 * recommended usage is as either a positive or negative acknowledgement
 * of a replication message:
 *
 * - If the entries were successfully appended to or already present in
 *   the log, then the node responds by identifying the last entry in the
 *   sequence as a potential anchor point.
 * - If they were not successfully appended then the node responds by
 *   indicating what the correct term number would be for using the
 *   specified index as an anchor, or if that index is not usable at all
 *   due to being beyond the end of the log, the index and term of the
 *   last entry which is present.
 *
 * Current practice is for all nodes should acknowledge all replication
 * messages. This policy might change in the future, however there are
 * several cases where acknowledgement is known to be essential:
 *
 * - For log entries which are in the process of being committed,
 *   acknowledgements are needed for determining when replication to a
 *   majority of nodes has been achieved.
 * - For nodes which have lagged behind the majority and are in the
 *   process of catching up, positive acknowledgements may be necessary
 *   to reassure the leader that the catch-up messages are being accepted
 *   by at least one node.
 * - Negative acknowledgements are necessary in the case where log entries
 *   are missing from a node and the leader has no other reason to resend
 *   those entries.
 */
class anchor:
	public basic_message {
private:
	/** The index of the potential anchor. */
	index_type _anchor_index;

	/** The term of the potential anchor. */
	term_type _anchor_term;
public:
	message_type type() const override;
	operator std::string() const override;
	encoder& encode(encoder& enc) const override;

	/** Decode anchor message.
	 * @param dec a decoder for interpreting the message content
	 */
	anchor(decoder& dec);

	/** Construct anchor message.
	 * @param term the sender's current term
	 * @param sender_id the node ID of the sender
	 * @param anchor_index the index of the potential anchor
	 * @param anchor_term the term of the potential anchor
	 */
	anchor(term_type term, node_id_type sender_id,
		index_type anchor_index, term_type anchor_term);

	/** Get the index of the potential anchor.
	 * @return the index
	 */
	index_type anchor_index() const {
		return _anchor_index;
	}

	/** Get the term of the potential anchor.
	 * @return the term
	 */
	term_type anchor_term() const {
		return _anchor_term;
	}
};

}

#endif

