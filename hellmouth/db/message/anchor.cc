// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <sstream>

#include "hellmouth/db/encoder.h"
#include "hellmouth/db/decoder.h"
#include "hellmouth/db/message/anchor.h"

namespace hellmouth::db::message {

message_type anchor::type() const {
	return message_type::anchor;
}

anchor::operator std::string() const {
	std::ostringstream out;
	out << "anchor("
		<< term() << ','
		<< sender_id() << ','
		<< _anchor_index << ','
		<< _anchor_term << ')';
	return out.str();
}

encoder& anchor::encode(encoder& enc) const {
	basic_message::encode(enc);
	enc << _anchor_index << _anchor_term;
	return enc;
}

anchor::anchor(decoder& dec):
	basic_message(dec) {

	dec >> _anchor_index >> _anchor_term;
}

anchor::anchor(term_type term, node_id_type sender_id,
	index_type anchor_index, term_type anchor_term):
	basic_message(term, sender_id),
	_anchor_index(anchor_index), _anchor_term(anchor_term) {}

}
