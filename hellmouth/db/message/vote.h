// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_MESSAGE_VOTE
#define HELLMOUTH_DB_MESSAGE_VOTE

#include "hellmouth/db/types.h"
#include "hellmouth/db/message/basic_message.h"

namespace hellmouth::db::message {

/** A message class for casting a vote in a leadership election.
 * Whenever a node nominates itself as a candidate to become leader, other
 * voting nodes should respond by casting a vote in favour provided that:
 *
 * - the voter has not already progressed to a higher term number, and
 * - the voter has not already voted for a different candidate for the
 *   same term.
 *
 * When a candidate nominates itself, it is implicitly presumed to have
 * voted for itself, and cannot therefore vote again for a different
 * candidate for the same term.
 *
 * The term number of the vote cast must match the term number of the
 * nomination, but no special action should be needed to achieve this:
 *
 * - If the current term number of the voter was higher then the nomination
 *   should have been disregarded.
 * - If the current term number of the voter was lower then the voter
 *   should have updated its current term number before casting its vote.
 *
 * Differences from vanilla Raft:
 *
 * - There is no provision for explicitly voting against a candidate, since
 *   abstaining achieves the same outcome with less network traffic.
 * - Because votes are multicast, they explicitly identify the candidate
 *   for which the vote has been cast.
 */
class vote:
	public basic_message {
private:
	/** The node ID of the candidate voted for. */
	node_id_type _candidate_id;
public:
	message_type type() const override;
	operator std::string() const override;
	encoder& encode(encoder& enc) const override;

	/** Decode vote message.
	 * @param dec a decoder for interpreting the message content
	 */
	vote(decoder& dec);

	/** Construct vote message.
	 * @param term the voter's term
	 * @param sender_id the node ID of the sender/voter
	 * @param candidate_id the node ID of the candidate voted for
	 */
	vote(term_type term, node_id_type sender_id,
		node_id_type candidate_id);

	/** Get the node ID of the voter.
	 * @return the node ID
	 */
	node_id_type voter_id() const {
		return sender_id();
	}

	/** Get the node ID of the candidate voted for.
	 * @return the node ID
	 */
	node_id_type candidate_id() const {
		return _candidate_id;
	}
};

}

#endif
