// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include "hellmouth/db/decoder.h"

namespace hellmouth::db {

void decoder::_end_of_data() {
	throw std::invalid_argument("unexpected end of data");
}

}
