// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_NODE
#define HELLMOUTH_DB_NODE

#include <set>
#include <filesystem>

#include "hellmouth/log/logger.h"
#include "hellmouth/event/timeout.h"
#include "hellmouth/event/file_descriptor.h"
#include "hellmouth/event/manager.h"
#include "hellmouth/crypto/ed25519/key_pair.h"
#include "hellmouth/db/types.h"
#include "hellmouth/db/config.h"
#include "hellmouth/db/follower.h"
#include "hellmouth/db/log.h"
#include "hellmouth/db/log_cache.h"
#include "hellmouth/db/snapshot_store.h"
#include "hellmouth/db/local_state.h"
#include "hellmouth/db/database.h"
#include "hellmouth/db/server.h"
#include "hellmouth/db/message/bus.h"
#include "hellmouth/db/message/fwd.h"

namespace hellmouth::db {

/** A class to represent a node within a database cluster. */
class node:
	public database {
private:
	/** The node configuration. */
	config _cfg;

	/** The ED25519 public-private key pair for this node.
	 * [TODO]: should be persistent.
	 */
	crypto::ed25519::key_pair _keypair;

	/** An event manager for operating the main event loop. */
	event::manager _em;

	/** The current operating mode of this node. */
	operating_mode _mode = operating_mode::follower;

	/** The current term number.
	 * This should be updated whenever a message is observed with a
	 * higher term number.
	 */
	term_type _current_term = 0;

	/** The candidate voted for in the current term, or 0 if not voted. */
	node_id_type _voted_for = 0;

	/** The votes cast for this node during the current term. */
	std::set<node_id_type> _votes;

	/** Follower status, indexed by node ID. */
	std::map<node_id_type, follower> _followers;

	/** The index of the next log entry which should be replicated. */
	index_type _next_index = 1;

	/** The index of the last log entry known to have been committed. */
	index_type _commit_index = 0;

	/** The local log. */
	log _log;

	/** A cache of replication messages which cannot be appended yet. */
	log_cache _log_cache;

	/** The snapshot store. */
	snapshot_store _snapshots;

	/** The local state of the database. */
	local_state _state;

	/** A server for handling client connections. */
	db::server _server;

	/** An event handler for new connections to the server. */
	event::file_descriptor _server_event;

	/** The message bus for communicating with other nodes. */
	message::bus _bus;

	/** An event handler for receiving messages from the bus. */
	event::file_descriptor _bus_event;

	/** An event handler for calling elections. */
	event::timeout _election_timeout;

	/** An event handler for sending heartbeats. */
	event::timeout _heartbeat_timeout;

	/** Accept a new client connection. */
	void accept_client_connection();

	/** Become follower, if not already. */
	void become_follower();

	/** Become leader, if not already. */
	void become_leader();

	/** Begin a new term.
	 * This function should be called when a message is seen with a
	 * more recent term than the current one, and also when this
	 * node wishes to call an election.
	 *
	 * @param term the new term number
	 */
	void begin_new_term(term_type term);

	/** Handle a nomination message.
	 * @param msg the message to be handled
	 */
	void handle(message::nomination& req);

	/** Handle a vote message.
	 * @param msg the message to be handled
	 */
	void handle(message::vote& req);

	/** Handle a replication message.
	 * @param msg the message to be handled
	 */
	void handle(message::replication& req);

	/** Handle an anchor message.
	 * @param msg the message to be handled
	 */
	void handle(message::anchor& req);

	/** Handle a proposal message.
	 * @param msg the message to be handled
	 */
	void handle(message::proposal& req);

	/** Handle any type of message from the bus.
	 * @param msg the message to be handled
	 */
	void handle(message::basic_message& msg);

	/** Receive and handle any type of message from the bus. */
	void receive_message();

	/** Handle an election timeout event. */
	void handle_election_timeout();

	/** Send a heartbeat message. */
	void send_heartbeat();

	/** Replicate from a specific log entry.
	 * The current implementation replicates a single log entry
	 * only, however multiple enties could be replicated where
	 * appropriate.
	 *
	 * @param index the log entry from which to replicate
	 */
	void replicate(index_type index);

	/** Handle a heartbeat timeout event. */
	void handle_heartbeat_timeout();

	/** Update commit index using match indexes of followers. */
	void update_commit_index();

	/** Apply committed log entries to state machine. */
	void apply_committed_log_entries();
public:
	data::any get(std::string_view path) override;
	void patch(std::string_view path, const data::any& cset) override;
	void patch(const data::any& cset, bool nowait = false);
	data::any status() override;

	/** Construct node.
	 * @param cfg the node configuration
	 * @param logger the audit log
	 */
	node(const config& cfg);

	/** Run this node in the foreground. */
	void run();
};

}

#endif
