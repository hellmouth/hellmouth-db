// This file is part of the HELLMOUTH cluster system.
// Copyright 2024 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>
#include <iterator>
#include <format>

#include <fcntl.h>

#include "hellmouth/db/encoder.h"
#include "hellmouth/db/decoder.h"
#include "hellmouth/db/log_file.h"

namespace hellmouth::db {

log_file::log_file(const std::filesystem::path& dirname,
	index_type base, size_t max_psize):
	_pathname(dirname / std::format("{:016x}.log", base)),
	_fd(_pathname, O_RDWR | O_CREAT | O_APPEND, 0600),
	_base(base),
	_max_psize(max_psize) {

	size_t remaining = _fd.size();
	while (remaining > 0) {
		size_t offset = _fd.seek(0, SEEK_CUR);
		uint16_t length = 0;
		_fd.read(&length, sizeof(length));
		octet_string entry;
		entry.resize(length);
		_fd.read(entry.data(), length);
		_offsets.push_back(offset);
		remaining -= sizeof(length) + length;
	}
	_fd.fsync();
}

size_t log_file::psize() const {
	return _fd.seek(0, SEEK_END);
}

octet_string log_file::at(index_type index) const {
	_fd.seek(_offsets.at(index - _base));
	uint16_t length = 0;
	_fd.read(&length, sizeof(length));
	octet_string entry;
	entry.resize(length);
	_fd.read(entry.data(), length);
	return entry;
}

void log_file::push_back(octet_string_view entry) {
	uint16_t length = entry.length();
	if (length != entry.length()) {
		throw std::length_error("log entry too large");
	}

	size_t offset = _fd.seek(0, SEEK_END);
	bool full = (offset + length + sizeof(length) > _max_psize);
	if (full && !_offsets.empty()) {
		throw log_file_full();
	}

	_fd.write(&length, sizeof(length));
	_fd.write(entry.data(), length);
	_fd.fdatasync();
	_offsets.push_back(offset);
}

void log_file::truncate(index_type index) {
	_fd.truncate(_offsets.at(index - _base));
	_fd.fdatasync();
}

void log_file::unlink() {
	std::filesystem::remove(_pathname);
}

}
